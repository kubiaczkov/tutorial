<?php
  // stworzenie połączenia z db
  $server = 'localhost'; # serwer
  $user = 'root'; # użytkownik
  $passwd = ''; # hasło
  $db = 'dane'; # baza danych
  // w przypadku niepowodzenia odpowiedni komunikat
  $conn = mysqli_connect($server, $user, $passwd, $db) or die('błąd podczas łączenia');

  # ... operacja na bazie danych

  // zamknięcie połączenia
  mysqli_close($conn);
 ?>
