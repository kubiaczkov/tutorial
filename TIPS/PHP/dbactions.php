<?php
# MYSQLI PROCEDURAL CRUD

# pobranie wartości z formularza za pomocą ... $_POST
  $zmienna = $_POST["atrybut_name_pola"];
  // sprawdzenie czy została podana
  if (!empty($zmienna)) {
    # atrybut podany
  } else {
    # atrybut nie podany
  }

  // identycznie w momencie kilku argumentów
  $zmienna_druga = $_POST["kolejny"];
  $zmienna_trzecia = $_POST["next"];

  if(!empty($zmienna_druga) && !empty($zmienna_trzecia)) {
    # oba atrybuty zostały podane
  } else {
    # nie wszystkie zostały podane
  }

# OPERACJE NA BAZIE DANYCH
# ... ustalenie połączenia z db

# 1. Wyświetlanie informacji z bazy danych
$sql = "SELECT id, firstname, lastname FROM MyGuests";
$result = mysqli_query($conn, $sql);
// sprawdzenie czy liczba wyników jest większa od 0 tzn. czy są jakiekolwiek wyniki
if (mysqli_num_rows($result) > 0) {
    // wypisywanie danych wiersz po wierszu
    while($row = mysqli_fetch_assoc($result)) {
      // odwoływanie sie do konkretnego pola z db za pomocą $row["nazwa-kolumny"]
      // !!! nazwa-kolumny musi mieć identyczny zapis jak w bazie
      // możliwość wstrzykiwania jednocześnie do wyników kodu html np. <br>, <li>, <td>
      echo "id: " . $row["id"]. " - Name: " . $row["firstname"]. " " . $row["lastname"]. "<br>";
    }
// w przypadku gdy brak wyników
} else {
    echo "0 results";
}

# 2. Dodawanie rekordów do bazy danych
// przygotowanie zapytania zgodnie z zapisem SQL
$sql = "INSERT INTO MyGuests (firstname, lastname, email)
        VALUES ($zmienna, $druga_zmienna, '$zmienna_tekstowa')";
        # zmienna tekstowa musi być umieszczeona w ''
// jeżeli opcja dodawania się powiedzie
if (mysqli_query($conn, $sql)) {
  # zrób coś np. wyświetl komunikat
  echo "New record created successfully";
// w innym przypadku
} else {
  # wypisze błąd SQL np. zła nazwa kolumny lub zły typ argumentu
  echo "Error: " . $sql . "<br>" . mysqli_error($conn);
}

# 3. Usuwanie rekordów z bazy danych
// przygotowanie zapytania
$sql = "DELETE FROM MyGuests WHERE id=3";
// jeżeli opcja usuwania się powiedzie
if (mysqli_query($conn, $sql)) {
    echo "Record deleted successfully";
// w innym przypadku
} else {
    echo "Error deleting record: " . mysqli_error($conn);
}

# 4. Aktualizacja rekordów w bazie danych
// przygotowanie zapytania
$sql = "UPDATE MyGuests SET lastname='$lastnname' WHERE id=$updated_id";
// jeżeli opcja aktualizacji się powiedzie
if (mysqli_query($conn, $sql)) {
    echo "Record updated successfully";
// w innym przypadku
} else {
    echo "Error updating record: " . mysqli_error($conn);
}

 ?>
