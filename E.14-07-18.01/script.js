function srednia() {
    // pobranie ocen
    var sprawdz = false;

    // pobieramy wartość z pola
    const polak = document.getElementById('Polak').value;
    // zmieniamy stringa na float
    fPolak = parseFloat(polak);
    // sprawdzamy czy jest float -> jezeli nie zmieniamy wartosc zmiennej sprawdz
    if (isNaN(fPolak)) sprawdz = true;

    const nowak = document.getElementById('Nowak').value;
    fNowak = parseFloat(nowak);
    if (isNaN(fNowak)) sprawdz = true;

    const rysik = document.getElementById('Rysik').value;
    fRysik = parseFloat(rysik);
    if (isNaN(fRysik)) sprawdz = true;

    // sprawdz czy są puste lub poprawne
    if (polak == "" || nowak == "" || rysik == "" || sprawdz) {
        alert("wpisz poprawne dane");
        print.innerHTML = "NaN"
    } else {
        const print = document.getElementById('print');

        let srednia_ocen = (fPolak + fNowak + fRysik) / 3;
        // zaokraglenie do 2 msc po przecinku
        srednia_ocen = srednia_ocen.toPrecision(3);
        print.innerHTML = srednia_ocen;
    }
    // alert(`${polak} ${nowak} ${rysik}`)
}
