<?php
  // pobranie wartości z formularza metodą POST
  $tytuł = $_POST['title'];
  $gatunek = $_POST['genre'];
  $rok = $_POST['year'];
  $ocena = $_POST['rate'];

  // stworzenie połączenia z db
  $server = 'localhost'; # serwer
  $user = 'root'; # użytkownik
  $passwd = ''; # hasło
  $db = 'dane'; # baza danych
  // w przypadku niepowodzenia odpowiedni komunikat
  $conn = mysqli_connect($server, $user, $passwd, $db) or die('błąd podczas łączenia');

  // sprawdzenie czy wszystkie dane zostały podane
  // if($boolan || $boolean_dwa)
  // wykona się w przypadku gdy którykolwiek z parametrów przyjmie wartość TRUE (logiczny OR)
  if(empty($tytuł) || empty($gatunek) || empty($rok) || empty($ocena))
  {
    echo "Nie podano wszystkich danych dotyczących filmu";
  }
  // w innym przypadku 
  else
  {
    // zapytanie które użyjemy na db
    $query = "INSERT INTO filmy (id, gatunki_id, tytul, rok, ocena)
              VALUES (NULL, $gatunek, '$tytuł', $rok, $ocena)";
    // użycie metody mysqli_query
    if (mysqli_query($conn, $query)) {
        echo "Film $tytuł został dodany do bazy";
    } else {
        echo "Error: " . $conn . "<br>" . mysqli_error($conn);
    }
  }
  // zamknięcie połączenia
  mysqli_close($conn);
 ?>
