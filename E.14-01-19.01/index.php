<!DOCTYPE html>
<!-- NA PODSTAWIE Arkusz E.14-01-19.01
+ dodatkowe elementy -->
<html lang="pl">
  <head>
    <!-- obsługa polskich znaków utf-8 -->
    <meta charset="utf-8">
    <!-- znacznik tytułowy -->
    <title>Egzamin zawodowy</title>
    <!-- ikona -->
    <link rel="shortcut icon" href="IMAGES/favicon.ico" />
    <!-- podłączenie zewnętrznego arkusza stylów -->
    <link rel="stylesheet" href="CSS/style.css">
  </head>
  <body>
    <div class="kontener">
      <div id="lewa-sekcja">
        <div class="panel-lewy">
          <h3>Dostępne gatunki filmu</h3>
          <!-- uporządkowana lista (ordered list) -->
          <ol>
            <li>Sci-Fi</li>
            <li>animacja</li>
            <li>dramat</li>
            <li>horror</li>
            <li>komedia</li>
          </ol>
          <p>
            <!-- otwiera w bierzącym oknie -->
            <a href="IMAGES/kadr.jpg">Pobierz obraz</a>
            <!-- odnośnik typu mailto -->
            <br/><a href="mailto:joe@example.com">email me</a>
          </p>
          <p>
            <!-- otwiera się w nowym oknie -->
            <a href="http://repertuar-kin.pl" target="_blank">Sprawdź repertuar kin</a>
          </p>
        </div>
      </div>
      <div id="prawa-sekcja">
        <div class="pierwszy-prawy-panel">
          <h1>FILMOTEKA</h1>
        </div>
        <div class="drugi-prawy-panel">
          <form action="PHP/dodaj.php" method="post">
            <label for="title">Tytuł</label>
            <input type="text" name="title" id="title"><br/>
            <label for="genre">Gatunek filmu:</label>
            <input type="number" name="genre" id="genre"><br/>
            <label for="year">Rok produkcji:</label>
            <input type="number" name="year" id="year"><br/>
            <label for="rate">Ocena:</label>
            <input type="number" name="rate" id="rate"><br/>
            <!-- przyciski reset i submit -->
            <button type="reset" name="reset">CZYŚĆ</button>
            <button type="submit" name="submit">DODAJ</button>
          </form>
        </div>
        <div class="trzeci-prawy-panel">
          <!-- alt -> teksty wyświetlający kiedy zdjęcia nie znalezionio/ nie załadowano poprawnie -->
          <img src="IMAGES/kadr.jpg" alt="zdjęcia filmowe">
        </div>
      </div>
    </div>
    <div class="stopka">
      <p>
        Autor strony: 00000000000
      </p>
    </div>
  </body>
</html>
